/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/Apps/SparkFSApp/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/* -> c.code */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <ctype.h>
#include <time.h>

#include "os.h"
#include "wimp.h"
#include "sprite.h"
#include "wimpt.h"
#include "bbc.h"
#include "flex.h"
#include "kernel.h"

#include "Interface/SparkFS.h"

#include "wos.h"
#include "ram.h"
#include "sparkfs.h"
#include "fs.h"
#include "code.h"

/**************************************************************************/

char * buffer1=NULL;
char * buffer2=NULL;


void bufferfp1(FILE * fp)
{
 if(!buffer1) flex_alloc((flex_ptr)&buffer1,0x8000);
 if(buffer1)  setvbuf(fp,buffer1,_IOFBF,0x8000);
}


void losebuffer1(void)
{
 if(buffer1) flex_free((flex_ptr)&buffer1);
}


void bufferfp2(FILE * fp)
{
 if(!buffer2) flex_alloc((flex_ptr)&buffer2,0x8000);
 if(buffer2)  setvbuf(fp,buffer2,_IOFBF,0x8000);
}


void losebuffer2(void)
{
 if(buffer2) flex_free((flex_ptr)&buffer2);
}


/**************************************************************************/


char arcname[256];
char newname[256];




char * xfgets(char * str,int num,FILE * stream)
{
 char * p;
 int c;
 int d;
 int max;

 p=str;
 max=num-1;

 while(--num)
 {
  c=fgetc(stream);

  if(c==EOF || ferror(stream))
  {
   if(num==max) str=NULL;
   break;
  }

  *p=c;

  if(c=='\n' || c=='\r')
  {
   if(c=='\r') *p='\n';
   d=fgetc(stream);
   if(!((d=='\n' || d=='\r') && c!=d)) ungetc(d,stream);
   p++;
   break;
  }
  p++;
 }

 *p=0;
 return(str);
}



#define streq(s0, s1)  strcmp(s0, s1) == 0



int quickdecotype(char * name)
{
 FILE * fp;
 int    hdr;
 int    code;

 code=0;
 hdr=0;

 fp=fopen(name,"rb");
 if(fp)
 {
  if(fread(&hdr,1,2,fp)==2)
  {
   if(hdr==0x9D1F) code=1;
   else
   if(hdr==0x8B1F) code=2;
  }
  fclose(fp);
 }

 return(code);
}





int findfiletype(char * name)
{
 FILE * f;
 char buf[512];
 int  type;

 type=getarctype(name);
 if(type!=-1) return(ARCHIVE);

 f=fopen(name,"rb");
 if(!f) return(NOTYPE);

 fread(buf,1,sizeof(buf),f);
 if(ferror(f))
 {
  fclose(f);
  return(NOTYPE);
 }
 else
 {
  fclose(f);
 }

 if(*buf==0x1F && *(buf+1)==0x9D)  return(COTYPE);
 else
 if(*buf==0x1F && *(buf+1)==0x8B)  return(GZIPTYPE);



 f=fopen(name,"r");
 if(!f) return(NOTYPE);

 while(1)
 {
  if((xfgets(buf,sizeof(buf),f)==NULL) || ferror(f))
  {
   type=NOTYPE;
   break;
  }


  if(strncmp(buf,"xbtoa Begin\n",12)==0)
  {
   type=ABTYPE;
   break;
  }

  if(strncmp(buf,"begin",5)==0)
  {
   type=UUTYPE;
   break;
  }

  if(strncmp(buf,"(This file must be converted with BinHex 4.0)",45)==0)
  {
   type=HQXTYPE;
   break;
  }

/*  if(cstrncmp(buf,"Content-Transfer-Encoding: Base64",8)==0)
  {
   type=MIMETYPE;
   break;
  } */

 }

 fclose(f);
 return(type);
}




int loadanyfile(char * name,int xvolatile,int type)
{
 int  keepfile=0;
 char fname[256];
 char rname[256];
 BOOL didnothing=TRUE;


 strcpy(fname,name);

 while(1)
 {
  strcpy(rname,fname);

  if(ISARCHIVETYPE(type))
  {
   /* open filer */

   openarcfiler(/*type,*/rname);

   break;
  }
  else
  {
   type=findfiletype(fname);

   if(ISARCHIVETYPE(type))
   {
    setftype(fname,type);
   }
   else
   if(type==ABTYPE)
   {
    if(!atobfile(fname)) break;
   }
   else
   if(type==UUTYPE)
   {
    if(!uudecodefile(fname)) break;
   }
   else
   if(type==COTYPE)
   {
    if(!uncompress(fname))   break;
   }
   else
   if(type==GZIPTYPE)
   {
    if(!ungzip(fname))   break;
   }
   else
   if(type==HQXTYPE)
   {
    if(!un_hqx(fname))        break;
   }
   else
   if(type==MIMETYPE)
   {
    if(!demime(fname))        break;
   }
   else
   {
    if(didnothing)        return(0);
                              break;
   }
  }

  if(xvolatile && strcmp(rname,fname)) remove(rname);
  didnothing=FALSE; /* by now we have done something */
 }

 if(xvolatile && !keepfile)            remove(fname);
 if(xvolatile && strcmp(rname,fname))  remove(rname);

 return(1);
}
