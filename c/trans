/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/Apps/SparkFSApp/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.trans */

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

#include "os.h"
#include "wimp.h"
#include "bbc.h"
#include "flex.h"

#include "wos.h"
#include "fsx.h"

static char * text;
static int    size;

/* merge in a new translation table */

os_error * addtranstable(char * name)
{
 os_error * err;
 fstat      f;
 int        fh;
 char     * p;

 err=stat(name,&f);
 if(!err)
 {
  if(!f.object)
  {
   errorbox("Messages not found");
  }
  else
  {
   flex_extende((flex_ptr)&text,size+f.length+2);
   if(!err)
   {
    err=fs_open(name,'r',&fh);
    if(!err)
    {
     p=text+size;
     err=fs_read(fh,p,f.length);
     if(!err)
     {
      *(p+f.length)=0;
      *(p+f.length+1)=0;
      size+=f.length;

      while(1)
      {
       while(*p && *p!=':') p++;
       if(!*p) break;
       *p++=0;
       while(*p && *p>=31) p++;
       *p++=0;
       if(!*p) break;
      }
     }
     err=fs_close(fh,err);
    }
   }
  }
 }

 return(err);
}



char * transtoken(char * token)
{
 char * p;
 char * q;
 int    len;

 if(text)
 {
  p=text;

  while(1)
  {
   q=p;
   len=strlen(p);
   if(!len)
   {
    p=token;
    break;
   }
   p+=len+1;
   if(!strcmp(token,q)) break;
   len=strlen(p);
   if(!len)
   {
    p=token;
    break;
   }
   p+=len+1;
  }
 } else p=token;

 return(p);
}




/* translate the text in the buffer */

os_error * trans(char * buffer,int maxlen)
{
 char * p;
 char * q;
 char   token[32];
 int    size;
 int    len;
 int    delta;


 p=buffer;
 size=strlen(buffer);

 while(*p)
 {
  if(*p++=='{')
  {
   if(!*p) break;
   else
   if(*p=='{')
   {
    memmove(p-1,p,size-(p-buffer)+1);
    size--;
    p++;
   }
   else  /* a token */
   {
    q=token;
    while(*p && *p!='}') *q++=*p++;
    if(!*p) break;
    *q=0;
    p++;
    q=transtoken(token);
    len=strlen(q);

    delta=len-(2+strlen(token));
    if((size+delta)<maxlen)
    {
     memmove(p+delta,p,size-(p-buffer)+1);
     p+=delta-len;
     size+=delta;
     memcpy(p,q,len);
    }
   }
  }
 }
 return(NULL);
}

os_error * inittrans(void)
{
 os_error * err;

 flex_alloce((flex_ptr)&text,0);
 size=0;
 err=addtranstable("<SparkFSRes$Dir>.Messages");

 return(err);
}
