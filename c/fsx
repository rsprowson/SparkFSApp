/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/Apps/SparkFSApp/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.fsx */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

#include "swis.h"
#include "kernel.h"
#include "bbc.h"
#include "os.h"
#include "wimp.h"

#include "wos.h"
#include "fsx.h"

os_error * fs_write(int fh,void * b,int n)
{
 os_regset rx;
 os_error * err;

 if(!n) return(NULL);

 rx.r[0]=2;
 rx.r[1]=fh;
 rx.r[2]=(int)b;
 rx.r[3]=n;

 err=os_swix(OS_GBPB,&rx);

 return(err);
}


os_error * fs_read(int fh,void * b,int n)
{
 os_error * err;
 os_regset  rx;

 rx.r[0]=4;
 rx.r[1]=fh;
 rx.r[2]=(int)b;
 rx.r[3]=n;

 err=os_swix(OS_GBPB,&rx);

 return(err);
}


os_error * fs_close(int fh,os_error * ep)
{
 os_regset  rx;
 os_error * err;

 rx.r[0]=0;
 rx.r[1]=fh;

 err=os_swix(OS_Find,&rx);

 if(!ep) return(err);
 else    return(ep);
}



os_error * fs_open(char * name,int mode,int * fh)
{
 os_regset  rx;
 os_error * err;
 fstat      f;

 if(mode=='r') rx.r[0]=0x4F;
 else
 if(mode=='w') rx.r[0]=0x8F;
 else
 if(mode=='u')
 {
  err=stat(name,&f);
  if(!err)
  {
   if(f.object) rx.r[0]=0xCF;
   else         rx.r[0]=0x8F;
  }
 }

 rx.r[1]=(int)name;

 err=os_swix(OS_Find,&rx);

 *fh=rx.r[0];

 return(err);
}



char * fs_typestring(int type)
{
 os_regset rx;
 int       i;
 static  char string[16];

 rx.r[0]=18;
 rx.r[2]=type;
 os_swix(OS_FSControl,&rx);

 *((int*)string)=rx.r[2];
 *((int*)(string+4))=rx.r[3];
 for(i=0;i<8;i++) if(string[i]<=32) break;
 string[i]=0;

 return(string);
}



os_error * fs_canonicalpath(char * path,char * buffer,int len,
                                       char * pathvar,char * pathstring)
{
 os_regset rx;

 rx.r[0]=37;
 rx.r[1]=(int)path;
 rx.r[2]=(int)buffer;
 rx.r[3]=(int)pathvar;
 rx.r[4]=(int)pathstring;
 rx.r[5]=len;

 return(os_swix(OS_FSControl,&rx));
}
